package modul5;
import java.awt.*;
import java.awt.event.*;
public class CloseFrame2 extends Frame {
    Label label;
    CFListener w = new CFListener(this);
    CloseFrame2(String title){
        super(title);
        label = new Label("Close the frame");
        this.addWindowListener(w);
    }
    void launchFrame(){
        setSize(300, 300);
        setVisible(true);
    }
    public static void main(String [] args){
        CloseFrame2 cf = new CloseFrame2("Close Window Example");
        cf.launchFrame();
    }
}
class CFListener extends WindowAdapter{
    CloseFrame2 ref;
    CFListener(CloseFrame2 ref){
        this.ref = ref;
    }
    public void windowClosing(){
      ref.dispose();
      System.exit(1);
}
}
