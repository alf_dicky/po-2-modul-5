import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CloseFrame5 extends Frame {
    Label label;
    JButton button;
    JPanel panel;
   CloseFrame5(String title){
        super(title);
        panel = new JPanel();
        label = new Label("Close the Frame");
        button = new JButton("I'm Swing button");
        button.addActionListener(new CFListener1());
        this.addWindowListener(new CFListener());
    }

    void lauchFrame(){
        panel.add(button);

        add(panel);
        setSize(300,300);
        setVisible(true);
    }

    class CFListener extends WindowAdapter{
        public void windowClosing(WindowEvent e){
            dispose();
            System.exit(1);
        }
    }

    class CFListener1 extends MouseAdapter implements ActionListener {

        public void actionPerformed(ActionEvent actionEvent) {
            String msg = "Button Cliked";
            button.setText(msg);
        }
    }

    public static void main(String args[]){
        CloseFrame5 cf = new CloseFrame5("Close Window Example");
        cf.lauchFrame();
    }
}
